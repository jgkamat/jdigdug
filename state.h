

#ifndef STATE_H_
#define STATE_H_

// State enum definition
typedef enum {
	START,
	STARTW,
	STARTT,
	STARTN,
	GAME3,
	GAME3W,
	GAME3T,
	GAME2,
	GAME2W,
	GAME2T,
	GAME1,
	GAME1W,
	GAME1T,
	GAMEOVER,
	GAMEOVERW,
	GAMEOVERW2,
	HAPPY,
	HAPPYW,
	HAPPYW2,
} State;

void nextState(State* current);
void reset(State* current);

#endif
