
#include "state.h"


void nextState(State* current) {
	if (*current == START) {
		*current = STARTW;
	} else if (*current == STARTW) {
		*current = STARTN;
	} else if (*current == STARTT) {
		*current = GAME3;
	} else if (*current == STARTN) {
		*current = STARTT;
	} else if (*current == GAME3) {
		*current = GAME3W;
	} else if (*current == GAME3W) {
		*current = GAME3T;
	} else if (*current == GAME3T) {
		*current = GAME2;
	} else if (*current == GAME2) {
		*current = GAME2W;
	} else if (*current == GAME2W) {
		*current = GAME2T;
	} else if (*current == GAME2T) {
		*current = GAME1;
	} else if (*current == GAME1) {
		*current = GAME1W;
	} else if (*current == GAME1W) {
		*current = GAME1T;
	} else if (*current == GAME1T) {
		*current = GAMEOVER;
	} else if (*current == GAMEOVER) {
		*current = GAMEOVERW;
	} else if (*current == GAMEOVERW) {
		*current = GAMEOVERW2;
	} else if (*current == GAMEOVERW2) {
		*current = START;
	} else if (*current == HAPPY) {
		*current = HAPPYW;
	} else if (*current == HAPPYW) {
		*current = HAPPYW2;
	} else if (*current == HAPPYW2) {
		*current = START;
	}
}

void reset(State* current) {
	*current = START;
}
