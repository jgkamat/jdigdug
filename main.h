// Main.h

// Needed for u16 def among other things.
#include "mylib.h"
#include "state.h"

#define RGB(r, g, b) ((r) | (g) << 5 | (b) << 10)
#define RED RGB(31, 0, 0)
#define GREEN RGB(0, 31, 0)
#define BLUE RGB(0, 0, 31)
#define MAGENTA RGB(31, 0, 31)
#define CYAN RGB(0, 31, 31)
#define YELLOW RGB(31, 31, 0)
#define WHITE RGB(31, 31, 31)
#define ORANGE RGB(31, 18, 0)
#define BLACK 0
#define BG_COLOR RGB(2, 2, 2) // =)

#define B_UP  0
#define B_RIGHT 1
#define B_DOWN 2
#define B_LEFT 3

// How high the skyline is.
#define SKYLINE 9
#define SKYLINE_COLOR 0x04800

#define GRID_OFFSET(x, y, numcols) (OFFSET((x) * 10 + 1, (y) * 10 + 1, (numcols)))
#define GRID_CENTER_OFFSET(x, y, numcols) (OFFSET((x) * 10 + 5, (y) * 10 + 5, (numcols)))

#define ENEMY_BUFFER 1

// 3 * 10 (block width)
#define DD_RANGE 10
#define STUN_AMNT 100
#define ENEMY_HEALTH 100
#define DD_DAMAGE 20

#define GRID_SIZE 10

#define WAIT_TIME (60 * 5)


// enemy generator
#define GEN_ENEMY(xVal, yVal, nWidth, nHeight, nImage)					\
	{.x = ((xVal) * GRID_SIZE + 1), .y = ((yVal) * GRID_SIZE + 1), .width = (nWidth), .height = (nHeight), \
	 .stunCounter = 0, .oldBackground = 0x0, .image = (nImage), .oldDir = 99, .health = ENEMY_HEALTH, \
	 .startX = ((xVal) * GRID_SIZE + 1), .startY = ((yVal) * GRID_SIZE + 1), .startWidth = (nWidth), .startHeight = (nHeight)}


extern u16* videoBuffer;

// Represents the digdug dude
typedef struct {
	int x;
	int y;
	int dX;
	int dY;
	int width;
	int height;

	int startX;
	int startY;

	int oldX;
	int oldY;
	int oldWidth;
	int oldHeight;

	int rotation;
} DigDug;

// Represents the grid

typedef struct {
	// Width and height in pixels
	int width;
	int height;

	// The number of cols/rows
	int x;
	int y;
} Grid;

typedef struct {
	int x;
	int y;
	int width;
	int height;
} GeneratedPit;

// Represents an enemy
typedef struct {
	int x;
	int y;
	int width;
	int height;

	int startX;
	int startY;
	int startWidth;
	int startHeight;
	int health;

	int oldX;
	int oldY;
	int oldWidth;
	int oldHeight;
	int oldDir;
	int stunCounter;
	const u16* image;
	u16* oldBackground;
} Enemy;

// Prototypes
void refreshOldValues(DigDug* dd);
void edgeDetect (int* var, int* delta, int limit, int scaling, int buffer, int size);
void manageKeys(u16* keyPressed, u16* lastButtons, u16* oldKeyPressed, u16*, DigDug* dd, Grid* grid);
void updateRotation(int* rotation, int keyPressed, int oldKeyPressed);
void drawPits(const GeneratedPit* pits, int len);
void refreshOldValuesEnemy(Enemy* val, int len);
void drawEnemies(Enemy* enemies, int len);
void moveEnemies(Enemy* en, Grid* grid, int len, DigDug* player);
void clearEnemies(Enemy* enemy, int len);
void initEnemies(Enemy* arr, int len);
int insideOfDd(int x, int y, DigDug* dd);
int insideOfEnemy(int x, int y, Enemy*);
void detectCollision(DigDug* player, Enemy* en, int enemyLen, State* state);
int rectCollision(int x, int y, int width, int height, int x2, int y2, int width2, int height2);
void stunHandler(DigDug* dd, Enemy* enemies, int enemyLen);
void drawSprites(State* state, DigDug* dd);
void handleStateIncrease(State* state);
void resetGame(int);
void initDigDug(DigDug* dd);
