/**
 * Some small libraries for a VBA Game
 *
 * @author Jay Kamat
 * @version 1.0
 */

#include "mylib.h"
#include "dmacontroller.h"

u16* videoBuffer = (u16*) 0x6000000;

// Sets an individual pixel
void setPixel(int x, int y, u16 color) {
	videoBuffer[OFFSET(x, y, NUMCOLS)] = color;
}

// Sets an individual pixel
u16 getPixel(int x, int y) {
	return videoBuffer[OFFSET(x, y, NUMCOLS)];
}

// Draws a filled rectangle
void drawRect(int xCoord, int yCoord, int width, int height, volatile u16 color) {
	for (int y = 0; y < height; y++) {
		DMA[3].src = &color;
		DMA[3].dst = &videoBuffer[(yCoord + y) * NUMCOLS + (xCoord)];
		DMA[3].cnt = width | DMA_ON | DMA_SOURCE_FIXED;
	}
}

// Draws an image in mode 3, but 0 -> Transparent.
void drawTImage3(int x, int y, int width, int height, const u16* image) {
	for (int i = 0; i < height; i++) {
		for (int j = 0; j < width; j++) {
			if (image[i * width + j] != 0)
				videoBuffer[(y + i)* 240 + (x + j)] = image[i * width + j];
		}
	}
}

// Draws an image in mode 3
void drawImage3(int x, int y, int width, int height, volatile const u16* image) {
	for (int i = 0; i < height; i++) {
		DMA[3].src = &image[i * width];
		DMA[3].dst = &videoBuffer[(y + i) * NUMCOLS + (x)];
		DMA[3].cnt = width | DMA_ON | DMA_SOURCE_INCREMENT;
		// Non DMA Implemntation
		// for (int j = 0; j < width; j++) {
		// 	videoBuffer[(y + i)* 240 + (x + j)] = image[i * width + j];
		// }
	}
}

void drawImageRotation3(int x, int y, int width, int height, const u16* image, int rotation) {
	if (rotation == ROT_0 || rotation == ROT_02) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[i * width + j] != 0) {
					videoBuffer[(y + i)* 240 + (x + j)] = image[i * width + j];
				}
			}
		}
	} else if (rotation == ROT_FLIP || rotation == ROT_FLIP2) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[(i) * width + (width - 1 - j)] != 0) {
					videoBuffer[(y + i)* 240 + (x + j)] = image[(i) * width + (width - 1 - j)];
				}
			}
		}
	} else if (rotation == ROT_90) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[(width - 1 - j) * width + i] != 0) {
					videoBuffer[(y + i) * 240 + (x + j)] = image[(width - 1 - j) * width + i];
				}
			}
		}
	} else if (rotation == ROT_F90) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[j * width + i] != 0) {
					videoBuffer[(y + i)* 240 + (x + j)] = image[j * width + i];
				}
			}
		}

	} else if (rotation == ROT_270) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[j * width + (width - 1 - i)] != 0) {
					videoBuffer[(y + i)* 240 + (x + j)] = image[j * width + (width - 1 - i)];
				}
			}
		}
	} else if (rotation == ROT_F270) {
		for (int i = 0; i < height; i++) {
			for (int j = 0; j < width; j++) {
				if (image[(height - 1 - j) * width + (width - 1 - i)] != 0) {
					videoBuffer[(y + i)* 240 + (x + j)] = image[(height - 1 - j) * width + (width - 1 - i)];
				}
			}
		}
	}
}

unsigned int customRand() {
	static unsigned int z1 = 37232, z2 = 38592, z3 = 32842, z4 = 94208;
	unsigned int b;
	b  = ((z1 << 6) ^ z1) >> 13;
	z1 = ((z1 & 4294967294U) << 18) ^ b;
	b  = ((z2 << 2) ^ z2) >> 27;
	z2 = ((z2 & 4294967288U) << 2) ^ b;
	b  = ((z3 << 13) ^ z3) >> 21;
	z3 = ((z3 & 4294967280U) << 7) ^ b;
	b  = ((z4 << 3) ^ z4) >> 12;
	z4 = ((z4 & 4294967168U) << 13) ^ b;
	return (z1 ^ z2 ^ z3 ^ z4);
}

// Prevent screen tearing (occurs due to screen accessing the memory while our program is
void waitForVBlank() {
	while (SCANLINECOUNTER > 160);
	while (SCANLINECOUNTER < 160);
}
