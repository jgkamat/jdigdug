
/**
 * mylib.h
 */

#ifndef MYLIB_H_   // No recursion!
#define MYLIB_H_

// Make unsigned short ez
typedef unsigned short u16;

// Define some global variables and small funcs.
#define REG_DISPCNT *(u16*) 0x4000000
#define MODE3 3
#define BG2_ENABLE (1<<10)
#define NUMCOLS 240
#define NUMROWS 160

#define SCANLINECOUNTER *(volatile u16*) 0x4000006
#define BUTTON_A       (1<<0)
#define BUTTON_B       (1<<1)
#define BUTTON_SELECT  (1<<2)
#define BUTTON_START   (1<<3)
#define BUTTON_RIGHT   (1<<4)
#define BUTTON_LEFT    (1<<5)
#define BUTTON_UP      (1<<6)
#define BUTTON_DOWN    (1<<7)
#define BUTTON_R       (1<<8)
#define BUTTON_L       (1<<9)
#define ARROW_KEYS     ((BUTTON_UP | BUTTON_LEFT | BUTTON_RIGHT | BUTTON_DOWN))

#define KEY_PRESSED(key) (~(BUTTONS) & key)
#define KEY_PRESSED_X(buttons, key) ((((buttons) & ~(BUTTONS)) & key))

#define ROT_F90  0
#define ROT_FLIP  1
#define ROT_270  2
#define ROT_0  3
#define ROT_90  4
#define ROT_FLIP2 5
#define ROT_F270  6
#define ROT_02  7

// Needs to be volatile or the opimizer will mess with it
#define BUTTONS *(volatile u16*) 0x4000130

#define OFFSET(x, y, numcols) ((y) * (numcols) + (x))

// MATH
#define MAX(x, y) (((x) > (y)) ? (x) : (y))
#define MIN(x, y) (((x) < (y)) ? (x) : (y))

// Function prototypes.
void setPixel(int x, int y, u16 color);
u16 getPixel(int x, int y);
void drawRect(int xCoord, int yCoord, int width, int height, u16 color);
void drawImage3(int x, int y, int width, int height, volatile const u16* image);
void drawTImage3(int x, int y, int width, int height, const u16* image);
void drawImageRotation3(int x, int y, int width, int height, const u16* image, int rotation);
unsigned int customRand();
void waitForVBlank();

#endif
