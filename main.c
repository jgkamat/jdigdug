

// Remade DigDug Game for the GBA
// Author: Jay Kamat


#include <stdlib.h>
#include "main.h"
#include "img/background.h"
#include "img/ddsprite.h"
#include "img/enemy1.h"
#include "img/enemy2.h"
#include "img/startscreen.h"
#include "img/gameover.h"
#include "img/happy.h"
#include "dmacontroller.h"

// Variables
const int SCALING_FACTOR = 100;
const int MAX_SPEED = 100;
const int MOVEMENT_FACTOR = 20;


static DigDug dd = {.x = 0, .y = 0,
					.width = DDSPRITE_WIDTH, .height = DDSPRITE_HEIGHT,
					.oldX = 0, .oldY = 0, .oldWidth = 0, .oldHeight = 0, .rotation = ROT_0};

Enemy enemies[] = {GEN_ENEMY(16, 13, ENEMY1_WIDTH, ENEMY1_HEIGHT, enemy1),
				   GEN_ENEMY(3, 8, ENEMY1_WIDTH, ENEMY1_HEIGHT, enemy1),
				   GEN_ENEMY(21, 6, ENEMY1_WIDTH, ENEMY1_HEIGHT, enemy1),
				   GEN_ENEMY(18, 13, ENEMY1_WIDTH, ENEMY1_HEIGHT, enemy2),};
static const int enemyLen = sizeof(enemies) / sizeof(enemies[0]);

static Grid grid = {.width = NUMCOLS, .height = NUMROWS , .x = NUMCOLS / 10, .y = NUMROWS / 10};

static const GeneratedPit pits[] = {{.x = 3, .y = 7, .width = 1, .height = 3},
									{.x = 15, .y = 13, .width = 5, .height = 1},
									{.x = 21, .y = 5, .width = 1, .height = 5},
									{.x = NUMCOLS / 10 / 2 - 4, .y = NUMROWS / 10 / 4, .width = 7, .height = 1}};
static const int pitLen = sizeof(pits) / sizeof(pits[0]);

static State state = START;

int main(void) {
	// Init DD
	initDigDug(&dd);

	// Activate mode 3
	REG_DISPCNT = MODE3 | BG2_ENABLE;

	state = START;


	u16 keyPressed = B_RIGHT;
	u16 oldKeyPressed = B_RIGHT;
	u16 oldPysicalKeyPressed = B_RIGHT;
	u16 lastButtons = 0;
	u16 timer = WAIT_TIME;
	u16 updateGame = 0;

	// Startup
	refreshOldValues(&dd);
	refreshOldValuesEnemy(enemies, enemyLen);
	initEnemies(enemies, enemyLen);

	for (;;) {
		if (KEY_PRESSED(BUTTON_SELECT)) {
			reset(&state);
			timer = WAIT_TIME;
			continue;
		}

		if (state == START) {
			drawImage3(0, 0, STARTSCREEN_WIDTH, STARTSCREEN_HEIGHT, startscreen);
			handleStateIncrease(&state);
			continue;
		} else if (state == GAMEOVER) {
			drawImage3(0, 0, GAMEOVER_WIDTH, GAMEOVER_HEIGHT, gameover);
			handleStateIncrease(&state);
			continue;
		} else if (state == HAPPY) {
			drawImage3(0, 0, HAPPY_WIDTH, HAPPY_HEIGHT, happy);
			handleStateIncrease(&state);
			continue;
		} else if (state == STARTW || state == GAMEOVERW || state == HAPPYW) {
			if (KEY_PRESSED(BUTTON_START)) {
				handleStateIncrease(&state);
			}
		} else if (state == GAMEOVERW2 || state == HAPPYW2) {
			if (!KEY_PRESSED(BUTTON_START)) {
				handleStateIncrease(&state);
			}
		} else if (state == GAME1T || state == GAME2T || state == GAME3T || state == STARTT
		|| state == GAME1W || state == GAME2W || state == GAME3W) {
			if (timer <= 0) {
				timer = WAIT_TIME;
				handleStateIncrease(&state);

				// Reset keys
				keyPressed = B_RIGHT;
				oldKeyPressed = B_RIGHT;
				oldPysicalKeyPressed = B_RIGHT;
				lastButtons = 0;

				updateGame = 1;
			} else {
				timer--;
				waitForVBlank();
				continue;
			}
		} else if (state == STARTN) {
			updateGame = 1;
			handleStateIncrease(&state);
		}

		if (state == GAME1 || state == GAME2 || state == GAME3 || updateGame) {
			updateGame = 0;
			// *** MAIN GAMEPLAY ***
			manageKeys(&keyPressed, &lastButtons, &oldKeyPressed, &oldPysicalKeyPressed, &dd, &grid);

			dd.x += dd.dX;
			dd.y += dd.dY;

			edgeDetect(&dd.x, &dd.dX, SCALING_FACTOR, NUMCOLS, 0, dd.width);
			edgeDetect(&dd.y, &dd.dY, SCALING_FACTOR, NUMROWS, 0, dd.height);

			moveEnemies(enemies, &grid, enemyLen, &dd);

			stunHandler(&dd, enemies, enemyLen);

			detectCollision(&dd, enemies, enemyLen, &state);

			waitForVBlank();

			// Clear DigDug
			drawRect(dd.oldX / SCALING_FACTOR, dd.oldY / SCALING_FACTOR, dd.oldWidth, dd.oldHeight, BG_COLOR);
			if (dd.oldY / SCALING_FACTOR < SKYLINE)
				drawRect(dd.oldX / SCALING_FACTOR, 0, dd.width, SKYLINE, SKYLINE_COLOR);

			// Clear enemies

			clearEnemies(enemies, enemyLen);

			refreshOldValues(&dd);
			refreshOldValuesEnemy(enemies, enemyLen);

			drawSprites(&state, &dd);

			drawEnemies(enemies, enemyLen);

			// Draw DigDug
			drawImageRotation3(dd.x / SCALING_FACTOR, dd.y / SCALING_FACTOR, dd.width, dd.height, ddsprite, dd.rotation);
		}
	}
}

void initDigDug(DigDug* dd) {
	dd->x = ((NUMCOLS / GRID_SIZE / 2 - 1) * GRID_SIZE + 1) * SCALING_FACTOR;
	dd->y = ((NUMROWS / GRID_SIZE / 4) * GRID_SIZE + 1) * SCALING_FACTOR;
	dd->startX = dd->x;
	dd->startY = dd->y;
}

void handleStateIncrease(State* state) {
	nextState(state);
	if (*state == STARTT) {
		resetGame(1);
		drawImage3(0, 0, BACKGROUND_WIDTH, BACKGROUND_HEIGHT, background);
		drawPits(pits, pitLen);
	} else if (*state == GAME2T || *state == GAME3T) {
		resetGame(0);
	}
}

void resetGame(int hardreset) {
	dd.x = dd.startX;
	dd.y = dd.startY;
	dd.rotation = ROT_0;

	for(int i = 0; i < enemyLen; i++) {
		enemies[i].x = enemies[i].startX;
		enemies[i].y = enemies[i].startY;
		enemies[i].width = enemies[i].startWidth;
		enemies[i].height = enemies[i].startHeight;
		enemies[i].stunCounter = 0;
		// Do not reset health. Keep enemies dead.
		if (hardreset) {
			dd.oldX = dd.x;
			dd.oldY = dd.y;
			enemies[i].oldX = enemies[i].x;
			enemies[i].oldY = enemies[i].y;
			enemies[i].health = ENEMY_HEALTH;
		}
	}
}

void drawSprites(State* state, DigDug* dd) {
	// Clear Sprite Area.
	if(*state == GAME3 || *state == GAME2 || *state == GAME1)
		drawRect(NUMCOLS - dd->width * 3, 0, dd->width * 3, SKYLINE, SKYLINE_COLOR);

	// God forgive me
	switch(*state) {
	case GAME3:
		drawTImage3(NUMCOLS - dd->width * 3, 0, dd->width, dd->height, ddsprite);
	case GAME2:
		drawTImage3(NUMCOLS - dd->width * 2, 0, dd->width, dd->height, ddsprite);
	case GAME1:
		drawTImage3(NUMCOLS - dd->width * 1, 0, dd->width, dd->height, ddsprite);
		break;
	default:
		break;
	}
}

void initEnemies(Enemy* arr, int len) {
	for(int i = 0; i < len; i++) {
		if (arr[i].oldBackground != 0) {
			// Just in case. Could cause problems though (if not init'd to NULL)
			free(arr[i].oldBackground);
		}
		arr[i].oldBackground = malloc(sizeof(u16) * arr[i].height * arr[i].width);
	}
}

// Cool bouncing. We don't need this.
void edgeDetect (int* var, int* delta, int scaling, int limit, int buffer, int size) {
	// Bouncing in y
	if (*var / scaling < buffer) {
		*var = 0;
		*delta = 0;
	} else if (*var / scaling > limit - size - buffer) {
		*var = (limit - size - buffer) * scaling;
		*delta = 0;
	}

}

void refreshOldValues(DigDug* dd) {
	dd->oldX = dd->x;
	dd->oldY = dd->y;
	dd->oldWidth = dd->width;
	dd->oldHeight = dd->height;
}

// Run BEFORE drawing the new guy
void refreshOldValuesEnemy(Enemy* enArr, int len) {
	for (int k = 0; k < len; k++) {
		enArr[k].oldX = enArr[k].x;
		enArr[k].oldY = enArr[k].y;
		enArr[k].oldWidth = enArr[k].width;
		enArr[k].oldHeight = enArr[k].height;

		// Copy old data back. (from the new location).
		for (int j = 0; j < enArr[k].oldHeight; j++) {
			DMA[3].src = &videoBuffer[OFFSET(enArr[k].x, j + enArr[k].y, NUMCOLS)];
			DMA[3].dst = &enArr[k].oldBackground[j * enArr[k].width];
			DMA[3].cnt = enArr[k].width | DMA_ON | DMA_SOURCE_INCREMENT;
		}
		// Non DMA Implemenation
		// for (int i = 0; i < enArr[k].oldWidth; i++)
		// 	enArr[k].oldBackground[place++] = BG_COLOR;
		// 	// enArr[k].oldBackground[place++] = getPixel(i + enArr[k].x, j + enArr[k].y);

	}

}

// Manages the key inputs for a DigDug
void manageKeys(u16* keyPressed, u16* lastButtons, u16* oldKeyPressed, u16* oldPysicalKeyPressed, DigDug* dd, Grid* grid) {

	// First make sure we are in a valid row/column.
	u16 centerX = dd->x / SCALING_FACTOR + dd->width / 2;
	u16 centerY = dd->y / SCALING_FACTOR + dd->height / 2;

	u16 xMovementAllowed = (!(((centerY) + (grid->height / grid->y / 2))
							  % (grid->height / grid->y))
							|| *oldKeyPressed == B_LEFT || *oldKeyPressed == B_RIGHT);

	u16 yMovementAllowed = (!(((centerX) + (grid->width / grid->x / 2))
							  % (grid->width / grid->x))
							|| *oldKeyPressed == B_DOWN || *oldKeyPressed == B_UP);
	u16 killMovement = 1;

	// Check if only one button is pressed
	if ((((~BUTTONS) & ARROW_KEYS) & (((~BUTTONS) & ARROW_KEYS) - 1)) == 0 && (((~BUTTONS) & ARROW_KEYS) != 0)) {
		if (KEY_PRESSED(BUTTON_LEFT)) {
			*oldPysicalKeyPressed = B_LEFT;
		} else if (KEY_PRESSED(BUTTON_RIGHT)) {
			*oldPysicalKeyPressed = B_RIGHT;

		} else if (KEY_PRESSED(BUTTON_UP)) {
			*oldPysicalKeyPressed = B_UP;

		} else if (KEY_PRESSED(BUTTON_DOWN)) {
			*oldPysicalKeyPressed = B_DOWN;
		}
	}


	// If we are pressing a new button!
	if ((*lastButtons & ~BUTTONS) & (ARROW_KEYS)) {
		if ((*lastButtons & ~BUTTONS) & BUTTON_LEFT) {
			*oldPysicalKeyPressed = B_LEFT;
			if (xMovementAllowed) {
				*keyPressed = B_LEFT;
				dd->dX = -MAX_SPEED;
				dd->dY = 0;
				killMovement = 0;
			}
		}
		if ((*lastButtons & ~BUTTONS) & BUTTON_RIGHT) {
			*oldPysicalKeyPressed = B_RIGHT;
			if (xMovementAllowed) {
				*keyPressed = B_RIGHT;
				dd->dX = MAX_SPEED;
				dd->dY = 0;
				killMovement = 0;
			}
		}
		if ((*lastButtons & ~BUTTONS) & BUTTON_UP) {
			*oldPysicalKeyPressed = B_UP;
			if (yMovementAllowed) {
				*keyPressed = B_UP;
				dd->dY = -MAX_SPEED;
				dd->dX = 0;
				killMovement = 0;
			}
		}
		if ((*lastButtons & ~BUTTONS) & BUTTON_DOWN) {
			*oldPysicalKeyPressed = B_DOWN;
			if (yMovementAllowed) {
				*keyPressed = B_DOWN;
				dd->dY = MAX_SPEED;
				dd->dX = 0;
				killMovement = 0;
			}
		}
	} else {
		// No new key was pressed.
		if (KEY_PRESSED(BUTTON_LEFT) && *oldPysicalKeyPressed == B_LEFT && xMovementAllowed) {
			*keyPressed = B_LEFT;
			dd->dX = -MAX_SPEED;
			dd->dY = 0;
			killMovement = 0;
		} else if (KEY_PRESSED(BUTTON_RIGHT) && *oldPysicalKeyPressed == B_RIGHT && xMovementAllowed) {
			*keyPressed = B_RIGHT;
			dd->dX = MAX_SPEED;
			dd->dY = 0;
			killMovement = 0;
		} else if (KEY_PRESSED(BUTTON_UP) && *oldPysicalKeyPressed == B_UP && yMovementAllowed) {
			*keyPressed = B_UP;
			dd->dY = -MAX_SPEED;
			dd->dX = 0;
			killMovement = 0;
		} else if (KEY_PRESSED(BUTTON_DOWN) && *oldPysicalKeyPressed == B_DOWN && yMovementAllowed) {
			*keyPressed = B_DOWN;
			dd->dY = MAX_SPEED;
			dd->dX = 0;
			killMovement = 0;
		}
		// Fallthrough
		else if ((KEY_PRESSED(BUTTON_UP) || KEY_PRESSED(BUTTON_DOWN)) && (!yMovementAllowed)) {
			if ((dd->x - MAX_SPEED * 2) / SCALING_FACTOR < 0) {
				*oldKeyPressed = B_RIGHT;
			} else if (((dd->x + MAX_SPEED * 2) / SCALING_FACTOR) + dd->width > NUMCOLS) {
				*oldKeyPressed = B_LEFT;
			}

			if (*oldKeyPressed == B_LEFT) {
				*keyPressed = B_LEFT;
				dd->dX = -MAX_SPEED;
				dd->dY = 0;
				killMovement = 0;
			} else if (*oldKeyPressed == B_RIGHT) {
				*keyPressed = B_RIGHT;
				dd->dX = MAX_SPEED;
				dd->dY = 0;
				killMovement = 0;

			}
		} else if (((KEY_PRESSED(BUTTON_RIGHT) || KEY_PRESSED(BUTTON_LEFT))) && (!xMovementAllowed)) {
			if ((dd->y - MAX_SPEED * 2) / SCALING_FACTOR < 0) {
				*oldKeyPressed = B_DOWN;
			} else if (((dd->y + MAX_SPEED * 2) / SCALING_FACTOR) + dd->height > NUMROWS) {
				*oldKeyPressed = B_UP;
			}

			if (*oldKeyPressed == B_UP) {
				*keyPressed = B_UP;
				dd->dY = -MAX_SPEED;
				dd->dX = 0;
				killMovement = 0;
			} else if (*oldKeyPressed == B_DOWN) {
				*keyPressed = B_DOWN;
				dd->dY = MAX_SPEED;
				dd->dX = 0;
				killMovement = 0;
			}
		}
	}
	if (killMovement) {
		dd->dX = 0;
		dd->dY = 0;
	}

	updateRotation(&dd->rotation, *keyPressed, *oldKeyPressed);

	*oldKeyPressed = *keyPressed;
	*lastButtons = BUTTONS;
}

void updateRotation(int* rotation, int keyPressed, int oldKeyPressed) {
	if (keyPressed == oldKeyPressed) {
		return;
	}

	// Check if we are pressing right/left
	if (keyPressed == B_RIGHT) {
		*rotation = ROT_0;
		return;
	} else if (keyPressed == B_LEFT) {
		*rotation = ROT_FLIP;
		return;
	}
	// Check if we pressed left/right in our last key
	if (oldKeyPressed == B_RIGHT) {
		*rotation = ROT_0;
	} else if (oldKeyPressed == B_LEFT) {
		*rotation = ROT_FLIP;
	}


	// Check if dist 1, 2 away
	if ((oldKeyPressed + 1) % 4 == keyPressed) {
		// Edge case for <-- to ^
		if (*rotation == ROT_FLIP) {
			*rotation = ROT_FLIP2;
		}

		(*rotation)++;
	} else if (((4 + oldKeyPressed - 1) % 4) == keyPressed) {
		// Account for -1 % 4 -> 1, not 3
		(*rotation)--;

		// We need oldKeyPressed > keypressed so that the next if statement will ever run
	} else if ((oldKeyPressed + 2) % 4 == keyPressed && oldKeyPressed >= keyPressed) {
		(*rotation) += 2;
	} else if ((4 + oldKeyPressed - 2) % 4 == keyPressed) {
		(*rotation) -= 2;
	}

	// Guard against negatives
	*rotation = *rotation  + 8;
	// Guard against > 8
	*rotation = *rotation  % 8;

	// Pick primary orientations for digdug
	if (*rotation == ROT_FLIP) {
		*rotation = ROT_FLIP2;
	}
	if (*rotation == ROT_02) {
		*rotation = ROT_0;
	}
}

void drawPits(const GeneratedPit pits[], int len) {
	for(int i = 0; i < len; i++) {
		int width = (pits[i].width - 1) * 10 + 8;
		int height = (pits[i].height - 1) * 10 + 8;
		drawRect(pits[i].x * 10 + 1, pits[i].y * 10 + 1, width, height, BG_COLOR);
	}
}

void clearEnemies(Enemy* enemy, int len) {
	for(int i = 0; i < len; i++) {
		drawTImage3(enemy[i].oldX, enemy[i].oldY, enemy[i].oldWidth, enemy[i].oldHeight, enemy[i].oldBackground);
	}
}

void drawEnemies(Enemy* enemy, int len) {
	int anyDrawn = 0;
	for(int i = 0; i < len; i++) {
		if (enemy[i].health > 0) {
			drawTImage3(enemy[i].x, enemy[i].y, enemy[i].width, enemy[i].height, enemy[i].image);
			anyDrawn = 1;
		}
	}
	if (!anyDrawn) {
		// =)
		state = HAPPY;
	}
}

// All logic for moving enemies, and various other functions.
void moveEnemies(Enemy* en, Grid* grid, int len, DigDug* player) {
	for(int i = 0; i < len; i++) {
		if (en[i].health <= 0) {
			// Don't touch this one if you can
			continue;
		}
		// Don't move if stunned.
		if (en[i].stunCounter > 0) {
			en[i].stunCounter--;
			continue;
		} else {
			// Restore health if we can move
			en[i].health = ENEMY_HEALTH;
		}

		int randFactor = 1;
		int newX = en[i].x;
		int newY = en[i].y;

		u16 xMovementAllowed = (!(((en[i].y + en[i].height / 2) + (grid->height / grid->y / 2)) % (grid->height / grid->y)));
		u16 yMovementAllowed = (!(((en[i].x + en[i].width / 2) + (grid->width / grid->x / 2)) % (grid->width / grid->x)));

		newX = en[i].x;
		newY = en[i].y;

		int oldDir = en[i].oldDir;
		while (randFactor < 2) {

			// Left
			if ((getPixel(en[i].x - ENEMY_BUFFER, en[i].y + en[i].height / 2) == BG_COLOR ||
				 insideOfDd(en[i].x - ENEMY_BUFFER, en[i].y + en[i].height / 2, player))
				&& xMovementAllowed && en[i].oldDir != B_LEFT){
				if (customRand() % randFactor == 0) {
					newX = en[i].x - 1;
					newY = en[i].y;
					randFactor++;
					oldDir = B_LEFT;
				}
			}

			// Right
			if ((getPixel(en[i].x + en[i].width - 1 + ENEMY_BUFFER, en[i].y + en[i].height / 2) == BG_COLOR ||
				 insideOfDd(en[i].x + en[i].width - 1 + ENEMY_BUFFER, en[i].y + en[i].height / 2, player))
				&& xMovementAllowed && en[i].oldDir != B_RIGHT){
				if (customRand() % randFactor == 0) {
					newX = en[i].x + 1;
					newY = en[i].y;
					randFactor++;
					oldDir = B_RIGHT;
				}
			}

			// Up
			if ((getPixel(en[i].x + en[i].width / 2, en[i].y - ENEMY_BUFFER) == BG_COLOR ||
				 insideOfDd(en[i].x + en[i].width / 2, en[i].y - ENEMY_BUFFER, player))
				&& yMovementAllowed && en[i].oldDir != B_UP){
				if (customRand() % randFactor == 0) {
					newX = en[i].x;
					newY = en[i].y - 1;
					randFactor++;
					oldDir = B_UP;
				}
			}

			// Down
			if ((getPixel(en[i].x + en[i].width / 2, en[i].y + en[i].height - 1 + ENEMY_BUFFER) == BG_COLOR ||
				 insideOfDd(en[i].x + en[i].width / 2, en[i].y + en[i].height - 1 + ENEMY_BUFFER, player))
				&& yMovementAllowed && en[i].oldDir != B_DOWN){
				if (customRand() % randFactor == 0) {
					newX = en[i].x;
					newY = en[i].y + 1;
					randFactor++;
					oldDir = B_DOWN;
				}
			}

			if (en[i].oldDir == 99 && randFactor < 2) {
				// If we still don't move, we're stuck so don't move!
				break;
			} else if (randFactor < 2) {
				// Give us another shot!
				en[i].oldDir = 99;
			}
		}
		en[i].oldDir = (oldDir + 2) % 4;

		en[i].x = newX;
		en[i].y = newY;
	}
}

// Handles all stun stuff
void stunHandler(DigDug* dd, Enemy* en, int enemyLen) {
	static u16 lastButtons = ~0x0;

	// No A Key, no money.
	if (! KEY_PRESSED_X(lastButtons, BUTTON_A)) {
		lastButtons = BUTTONS;
		return;
	}

	// Already inverted.
	lastButtons = BUTTONS;

	for(int i = 0; i < enemyLen; i++) {
		if (dd->rotation == ROT_0 || dd->rotation == ROT_02) {
			for (int j = 0; j < DD_RANGE; j++) {
				int tx = j + dd->x / SCALING_FACTOR + dd->width;
				int ty = dd->y / SCALING_FACTOR + dd->height / 2;
				if (insideOfEnemy(tx, ty, &en[i])){
					// Valid
					en[i].stunCounter = STUN_AMNT;
					en[i].health = en[i].health - DD_DAMAGE;
					break;
				} else if(getPixel(tx, ty) != BG_COLOR) {
					// Not Valid
					break;
				}
			}
		} else if (dd->rotation == ROT_90 || dd->rotation == ROT_F90) {
			for (int j = 0; j < DD_RANGE; j++) {
				int tx =  dd->x / SCALING_FACTOR + dd->width / 2;
				int ty = j + dd->y / SCALING_FACTOR + dd->height;
				if (insideOfEnemy(tx, ty, &en[i])){
					// Valid
					en[i].stunCounter = STUN_AMNT;
					en[i].health = en[i].health - DD_DAMAGE;
					break;
				} else if(getPixel(tx, ty) != BG_COLOR) {
					// Not Valid
					break;
				}
			}
		} else if (dd->rotation == ROT_FLIP || dd->rotation == ROT_FLIP2) {
			for (int j = 0; j < DD_RANGE; j++) {
				int tx =  dd->x / SCALING_FACTOR - j;
				int ty = dd->y / SCALING_FACTOR + dd->height / 2;
				if (insideOfEnemy(tx, ty, &en[i])){
					// Valid
					en[i].stunCounter = STUN_AMNT;
					en[i].health = en[i].health - DD_DAMAGE;
					break;
				} else if(getPixel(tx, ty) != BG_COLOR) {
					// Not Valid
					break;
				}
			}
		} else if (dd->rotation == ROT_270 || dd->rotation == ROT_F270) {
			for (int j = 0; j < DD_RANGE; j++) {
				int tx =  dd->x / SCALING_FACTOR + dd->width / 2;
				int ty = dd->y / SCALING_FACTOR - j;
				if (insideOfEnemy(tx, ty, &en[i])){
					// Valid
					en[i].stunCounter = STUN_AMNT;
					en[i].health = en[i].health - DD_DAMAGE;
					break;
				} else if(getPixel(tx, ty) != BG_COLOR) {
					// Not Valid
					break;
				}
			}
		}
	}
}

// True -> Collision
// False -> Miss
int insideOfDd(int x, int y, DigDug* dd) {
	if (x < dd->x / SCALING_FACTOR || x > dd->x / SCALING_FACTOR + dd->width - 1
		|| y < dd->y / SCALING_FACTOR || y > dd->y / SCALING_FACTOR + dd->height - 1) {
		return 0;
	}
	return 1;
}

// True -> Collision
// False -> Miss
int rectCollision(int x, int y, int width, int height, int x2, int y2, int width2, int height2) {
	if ((x > x2 + width2 || x + width < x2) || (y + height < y2 || y > y2 + height2)) {
		return 0;
	}
	return 1;
}


// True -> Collision
// False -> Miss
int insideOfEnemy(int x, int y, Enemy* enemy) {
	return rectCollision(x, y, 1, 1, enemy->x, enemy->y, enemy->width, enemy->height);
}

// Handles collision automagically.
void detectCollision(DigDug* player, Enemy* en, int enemyLen, State* state) {
	for(int i = 0; i < enemyLen; i++) {
		// Check if any enemy intersects the player
		if (en[i].health <= 0)
			continue;

		if (rectCollision(player->x / SCALING_FACTOR, player->y / SCALING_FACTOR, player->width - 1, player->height - 1,
						  en[i].x, en[i].y, en[i].width - 1, en[i].height - 1)) {
			handleStateIncrease(state);
		}
	}
}
