# JDigDug

JDigDug is a lite rework of the classic DigDug game, made for the Game Boy Advance in mode 3.

### Controls

Control      | Keys
-------------|---------
Select       | Quit to menu
Start        | Forward select
D-Pad        | Movement
A            | Attack

The exact keys vary on your emulator.

### Gameplay

You have 3 lives, displayed in the top right, if you loose all of them, it's game over. Killing all the baddies wins the game!

The baddies currently only move randomly, but they move pretty quickly, so watch out!

### Compilation

You can compile with `make` and run in VBA with `make vba`.

### License

JDigDug is licensed under the GPLv3.
